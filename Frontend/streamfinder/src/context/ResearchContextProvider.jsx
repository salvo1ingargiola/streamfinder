import { useState } from "react";
import { ResearchContext } from "./ResearchContext";

export default function ResearchContextProvider({ children }) {
  const [list, setList] = useState([]); //tengo traccia del risultato della ricerca
  const [item, setItem] = useState([]); //tengo traccia dell'oggetto selezionato
  const [geo, setGeo] = useState(""); //tengo traccia della regione selezionata

  //tengo traccia dell'utente
  const [userData, setUserData] = useState({
    nome: "",
    cognome: "",
    email: "",
    ruoli: [""],
  });

  //tengo traccia dell'utente loggato
  const [isLogged, setIsLogged] = useState(false);

  //Tengo traccia dell'utente admin
  const [isAdmin, setIsAdmin] = useState(false);

  return (
    <ResearchContext.Provider
      value={{
        list,
        setList,
        item,
        setItem,
        geo,
        setGeo,
        userData,
        setUserData,
        isLogged,
        setIsLogged,
        isAdmin,
        setIsAdmin,
      }}
    >
      {children}
    </ResearchContext.Provider>
  );
}
