import Navbar from "../components/Navbar";
import Footer from "../components/Footer";
import { useOutlet } from "react-router-dom";

export default function Layout() {
  const outlet = useOutlet();

  return (
    <>
      <Navbar />
      {outlet}
      <Footer />
    </>
  );
}
