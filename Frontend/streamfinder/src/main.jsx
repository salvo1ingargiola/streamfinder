import React from "react";
import ReactDOM from "react-dom/client";
import App from "./App.jsx";
import "./index.css";
import "../node_modules/bootstrap/dist/js/bootstrap.bundle.js";
import "../node_modules/bootstrap/dist/css/bootstrap.css";
import { RouterProvider, createBrowserRouter } from "react-router-dom";
import Layout from "./pages/Layout.jsx";
import Homepage from "./pages/Homepage.jsx";
import ResearchContextProvider from "./context/ResearchContextProvider.jsx";
import ListResult from "./pages/ListResult.jsx";
import DetailsPage from "./pages/DetailsPage.jsx";
import NotFound from "./pages/NotFound.jsx";
import Register from "./pages/Register.jsx";
import Login from "./pages/Login.jsx";
import UserProfile from "./pages/UserProfile.jsx";
import Users from "./pages/Users.jsx";
import UserResearch from "./pages/UserResearch.jsx";
import UsersH from "./pages/UsersH.jsx";

const router = createBrowserRouter([
  {
    element: (
      <ResearchContextProvider>
        <Layout />
      </ResearchContextProvider>
    ),
    children: [
      {
        path: "/",
        children: [
          { path: "", element: <Homepage /> },
          { path: "/list", element: <ListResult /> },
          { path: "/details", element: <DetailsPage /> },
          { path: "*", element: <NotFound /> },
          { path: "/register", element: <Register /> },
          { path: "/login", element: <Login /> },
          { path: "/userProfile", element: <UserProfile /> },
          { path: "/users", element: <Users /> },
          { path: "/userResearch", element: <UserResearch /> },
          { path: "/allResearch", element: <UsersH /> },
        ],
      },
    ],
  },
]);

ReactDOM.createRoot(document.getElementById("root")).render(
  <React.StrictMode>
    <RouterProvider router={router}></RouterProvider>
  </React.StrictMode>
);
