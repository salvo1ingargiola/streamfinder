import { useContext } from "react";
import { ResearchContext } from "../context/ResearchContext";
import { Link, useNavigate } from "react-router-dom";
import Cookies from "../../node_modules/js-cookie";

export default function Navbar() {
  const {
    list,
    setList,
    item,
    setItem,
    geo,
    setGeo,
    userData,
    setUserData,
    isLogged,
    setIsLogged,
    isAdmin,
    setIsAdmin,
  } = useContext(ResearchContext);

  const navigate = useNavigate();

  const logout = () => {
    Cookies.remove("nome");
    Cookies.remove("cognome");
    Cookies.remove("email");
    Cookies.remove("ruoli");
    Cookies.remove("token");

    setIsLogged(false);
    setUserData({});
    navigate("/");
  };

  return ( 
    <nav
      className="navbar navbar-expand-lg navbar-light bg-secondary fixed-top"
      style={{ opacity: 0.8 }}
    >
      <div className="container-fluid">
        <Link className="navbar-brand" to="/">
          StreamFinder
        </Link>

        <button
          className="navbar-toggler"
          type="button"
          data-bs-toggle="collapse"
          data-bs-target="#navbarNav"
          aria-controls="navbarNav"
          aria-expanded="false"
          aria-label="Toggle navigation"
        >
          <span className="navbar-toggler-icon"></span>
        </button>

        <div
          className="collapse navbar-collapse justify-content-end"
          id="navbarNav"
        >
          <ul className="navbar-nav">
            {isLogged ? (
              <li className="nav-item">
                <Link to="/userProfile" className="nav-link text-white">
                  Profilo
                </Link>
              </li>
            ) : (
              ""
            )}
            {isLogged && isAdmin ? (
              <li className="nav-item">
                <Link to="/users" className="nav-link text-white">
                  Utenti
                </Link>
              </li>
            ) : (
              ""
            )}
            {isLogged ? (
              <li className="nav-item">
                <Link to="/userResearch" className="nav-link text-white">
                  Le tue ricerche
                </Link>
              </li>
            ) : (
              ""
            )}
            {isLogged && isAdmin ? (
              <li className="nav-item">
                <Link to="/allResearch" className="nav-link text-white">
                  Ricerche utenti
                </Link>
              </li>
            ) : (
              ""
            )}
            <li className="nav-item">
              {isLogged ? (
                <Link to="/">
                  <button onClick={logout} className="btn btn-dark">
                    Logout
                  </button>
                </Link>
              ) : (
                <Link to="/login" className="nav-link text-white">
                  Login
                </Link>
              )}
            </li>
          </ul>
        </div>
      </div>
    </nav>
  );
}
