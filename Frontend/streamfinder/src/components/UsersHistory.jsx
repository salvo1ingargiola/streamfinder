import React, { useContext, useEffect, useState } from "react";
import { ResearchContext } from "../context/ResearchContext";
import Cookies from "../../node_modules/js-cookie";

export default function UsersHistory() {
  const {
    list,
    setList,
    item,
    setItem,
    geo,
    setGeo,
    userData,
    setUserData,
    isLogged,
    setIsLogged,
    isAdmin,
    setIsAdmin,
  } = useContext(ResearchContext);

  const [ricerche, setRicerche] = useState([]); //Per la fetch delle ricerche
  //const [utenti, setUtenti] = useState([]); //Per la fetchUtenti

  useEffect(() => {
    if (isAdmin) {
      const fetchUsersAndResearch = async () => {
        try {
          //Fetch degli utenti
          const usersResponse = await fetch(
            "http://localhost:8080/api/utente/getUtenti",
            {
              method: "GET",
              headers: { Authorization: "Bearer " + Cookies.get("token") },
            }
          );
          if (!usersResponse.ok) {
            throw new Error("Errore nel recupero degli utenti");
          }
          const usersData = await usersResponse.json();
          //setUtenti(usersData);

          //Fetch delle ricerche
          const researchResponse = await fetch(
            "http://localhost:8080/api/streamInfo/getStreamInfos",
            {
              method: "GET",
              headers: { Authorization: "Bearer " + Cookies.get("token") },
            }
          );
          if (!researchResponse.ok) {
            throw new Error("Errore nel recupero delle ricerche");
          }
          const researchData = await researchResponse.json();

          //Combina le informazioni di ricerca con quelle degli utenti, se l'utente ha lo stesso id, ne memorizzo il nome e il cognome
          const combinedData = researchData.map((ricerca) => {
            const user = usersData.find((u) => u.id === ricerca.idu);
            return {
              ...ricerca,
              userName: user
                ? `${user.nome} ${user.cognome}`
                : "Utente sconosciuto",
            };
          });

          setRicerche(combinedData);
        } catch (error) {
          console.error(error);
        }
      };

      fetchUsersAndResearch();
    }
  }, [isAdmin]); //Esegue fetchUserAndResearch quando cambiano lo stato di admin

  //Se l'utente non è admin, non mostra nulla
  if (!isAdmin) {
    return <div>Accesso non autorizzato.</div>;
  }

  return (
    <div className="container mt-5">
      <h2 className="mb-4 text-center" style={{ color: "white" }}>
        Cronologia Ricerche Utenti
      </h2>
      <div className="row">
        {ricerche.length > 0 ? (
          ricerche.map((ricerca, index) => (
            <div key={index} className="col-lg-3 mb-3">
              <div className="card h-100">
                <div className="card-body">
                  <h5 className="card-title">{ricerca.title}</h5>
                  <p className="card-text">Geo: {ricerca.geo.toUpperCase()}</p>
                  <p className="card-text">Utente: {ricerca.userName}</p>
                </div>
              </div>
            </div>
          ))
        ) : (
          <p className="text-center">Nessuna ricerca trovata.</p>
        )}
      </div>
    </div>
  );
}
