export default function Footer() {
  return (
    <footer className="footer bg-secondary text-center text-lg-start fixed-bottom">
      <div className="text-center p-3" style={{ color: "white" }}>
        © 2024 StreamFinder: Tutti i diritti riservati
      </div>
    </footer>
  );
}
