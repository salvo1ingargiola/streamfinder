import { useContext, useEffect, useState } from "react";
import { ResearchContext } from "../context/ResearchContext";
import { Link } from "react-router-dom";

export default function UsersList() {
  const {
    list,
    setList,
    item,
    setItem,
    geo,
    setGeo,
    userData,
    setUserData,
    isLogged,
    setIsLogged,
    isAdmin,
    setIsAdmin,
  } = useContext(ResearchContext);

  const [utenti, setUtenti] = useState([]); //Per la gestione degli utenti

  useEffect(() => {
    async function fetchUtenti() {
      //Fetch che restituisce la lista utenti
      const response = await fetch(
        "http://localhost:8080/api/utente/getUtenti",
        {
          method: "GET",
        }
      );
      if (!response.ok) {
        console.log("Errore");
      } else {
        const data = await response.json();
        const utenti = data.map((utente) => ({
          id: utente.id,
          nome: utente.nome,
          cognome: utente.cognome,
          email: utente.email,
          ruolo: utente.ruoli.map((ruolo) => ruolo.nome),
        }));
        setUtenti(utenti);
      }
    }
    fetchUtenti();
  }, []);

  return (
    <div className="container">
      <h1 className="mb-4 text-center" style={{ color: "white" }}>
        Utenti registrati
      </h1>
      {isLogged && isAdmin ? (
        <div>
          <div className="row">
            {utenti.map((user, index) => (
              <div key={index} className="col-lg-4 mb-3">
                <div className="card">
                  <div className="card-body">
                    <h5 className="card-title">
                      {user.nome} {user.cognome}
                    </h5>
                    <p className="card-text">Email: {user.email}</p>
                    <span className="badge bg-primary">
                      {user.ruolo.join(", ")}
                    </span>
                  </div>
                </div>
              </div>
            ))}
          </div>
        </div>
      ) : (
        <div>
          <p>Non hai le autorizzazioni valide</p>
          <Link to="/" className="btn btn-dark">
            Torna alla home
          </Link>
        </div>
      )}
      <br />
    </div>
  );
}
