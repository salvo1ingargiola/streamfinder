import React, { useState, useContext, useEffect } from "react";
import { ResearchContext } from "../context/ResearchContext";
import { useNavigate } from "react-router-dom";

export default function HomeForm() {
  const {
    list,
    setList,
    item,
    setItem,
    geo,
    setGeo,
    userData,
    setUserData,
    isLogged,
    setIsLogged,
    isAdmin,
    setIsAdmin,
  } = useContext(ResearchContext);

  const [search, setSearch] = useState(""); //Tengo traccia della ricerca
  const navigate = useNavigate();
  const [region, setRegion] = useState([]); //Regioni in cui l'API funziona

  useEffect(() => {
    const fetchRegions = async () => {
      const url = "https://streaming-availability.p.rapidapi.com/countries";
      const options = {
        method: "GET",
        headers: {
          "X-RapidAPI-Key":
            "9ebbae1af8msh23c669d14ec6f94p176eacjsn9d72df12550c",
          "X-RapidAPI-Host": "streaming-availability.p.rapidapi.com",
        },
      };
      try {
        const response = await fetch(url, options);
        if (!response.ok) throw new Error("Errore nel recupero delle regioni");
        const data = await response.json();
        //Mappo i risultati per allinearli alla struttura esistente di `region`
        const regionsFormatted = Object.keys(data.result).map((key) => ({
          value: key,
          nome: data.result[key].name,
        }));
        setRegion(regionsFormatted);
      } catch (error) {
        console.error("Errore nella fetch delle regioni:", error);
      }
    };
    fetchRegions();
  }, []); //L'array vuoto assicura che l'effetto venga eseguito solo una volta, al montaggio del componente

  const handleChange = (e) => {
    const { name, value } = e.target;
    if (name === "search") setSearch(value);
    if (name === "lang") setGeo(value);
  };

  const handleSubmit = async (e) => {
    e.preventDefault();

    try {
      //Prima Fetch: recupero i risultati della ricerca
      const url = `https://streaming-availability.p.rapidapi.com/search/title?title=${search}&country=${geo}&show_type=all&output_language=en`;
      const response = await fetch(url, {
        method: "GET",
        headers: {
          "X-RapidAPI-Key":
            "9ebbae1af8msh23c669d14ec6f94p176eacjsn9d72df12550c",
          //"X-RapidAPI-Key" : "a4cc668aedmsha3091c6e65ccf98p1a191djsnfefa2730daf0",
          "X-RapidAPI-Host": "streaming-availability.p.rapidapi.com",
        },
      });

      if (!response.ok)
        throw new Error("Errore nel recupero dei risultati della ricerca");
      const { result } = await response.json();
      setList(result);

      //Seconda Fetch: ottengo l'ID dell'utente loggato
      const userResponse = await fetch(
        "http://localhost:8080/api/utente/getUtenti",
        {
          method: "GET",
        }
      );

      if (!userResponse.ok) throw new Error("Errore nel recupero degli utenti");
      const utenti = await userResponse.json();
      const utenteCorrispondente = utenti.find(
        (utente) => utente.email === userData.email
      );

      if (!utenteCorrispondente) throw new Error("Utente non trovato");

      //Terza Fetch: salvo i dettagli della ricerca nel database
      const saveResponse = await fetch(
        "http://localhost:8080/api/streamInfo/addStreamInfo",
        {
          method: "POST",
          headers: { "Content-Type": "application/json" },
          body: JSON.stringify({
            title: search,
            geo: geo,
            idu: utenteCorrispondente.id,
          }),
        }
      );

      if (!saveResponse.ok)
        throw new Error("Errore nel salvataggio della ricerca");
      alert("Dati della ricerca salvati con successo.");

      navigate("/list");
    } catch (error) {
      console.error(error.message);
    }
  };

  return (
    <div>
      <h1 style={{ textAlign: "center", color: "white" }}>
        Dove guardi stasera?👀
      </h1>
      <br />
      {isLogged ? (
        <form className="d-flex justify-content-center" onSubmit={handleSubmit}>
          <input
            className="form-control me-2"
            type="search"
            name="search"
            required
            value={search}
            onChange={handleChange}
            placeholder="Cerca film o serie"
            aria-label="Search"
          />
          <select
            name="lang"
            value={geo}
            onChange={handleChange}
            className="form-select me-2"
            required
          >
            <option value="" disabled>
              ---Where?---
            </option>
            {region.map((r) => (
              <option key={r.value} value={r.value}>
                {r.nome}
              </option>
            ))}
          </select>
          <button className="btn btn-success" type="submit">
            Cerca
          </button>
        </form>
      ) : (
        <h5 style={{ color: "white" }}>Effettua il login per scoprirlo!</h5>
      )}
    </div>
  );
}
