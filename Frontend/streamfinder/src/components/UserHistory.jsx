import React, { useContext, useEffect, useState } from "react";
import { ResearchContext } from "../context/ResearchContext";
import { useNavigate } from "react-router-dom";

export default function UserHistory() {
  const {
    list,
    setList,
    item,
    setItem,
    geo,
    setGeo,
    userData,
    setUserData,
    isLogged,
    setIsLogged,
    isAdmin,
    setIsAdmin,
  } = useContext(ResearchContext);

  const [ricerche, setRicerche] = useState([]); //Tengo traccia della lista ricerche che restituisce la fetch
  const navigate = useNavigate(); //Per navigare tra le pagine

  useEffect(() => {
    async function fetchUserAndResearch() {
      try {
        const usersResponse = await fetch(
          //Fetch che restituisce al lista degli utenti registrati
          "http://localhost:8080/api/utente/getUtenti",
          {
            method: "GET",
          }
        );
        if (!usersResponse.ok)
          throw new Error("Errore nella fetch degli utenti");
        const usersData = await usersResponse.json();
        //Trovo la corrispondenza tra la mail dell'utente loggato e quella memorizzata nel db per recuperare l'id utente
        const user = usersData.find((u) => u.email === userData.email);
        if (!user) throw new Error("Utente non trovato");

        const researchResponse = await fetch(
          //Da passare a questa fetch che restitusice la lista delle ricerche effettuate dall'utente
          `http://localhost:8080/api/streamInfo/getStreamInfos/${user.id}`
        );
        if (!researchResponse.ok)
          throw new Error("Errore nella fetch delle ricerche");
        const researchData = await researchResponse.json();
        setRicerche(researchData);
      } catch (error) {
        console.error(error);
      }
    }

    if (isLogged) {
      //Se è loggato effettua le fetch, altrimenti non visualizza nulla
      fetchUserAndResearch();
    }
  }, [isLogged, userData.email]); //Esegue fetchUserAndResearch quando cambiano lo stato di login e l'email dell'utente loggato

  //Quando viene effettuato il click su una card, viene effettuata nuovamente una ricerca con quel titolo e quella geolocalizzazione
  const handleCardClick = async (title, geo) => {
    try {
      const url = `https://streaming-availability.p.rapidapi.com/search/title?title=${title}&country=${geo}&show_type=all&output_language=en`;
      const response = await fetch(url, {
        method: "GET",
        headers: {
          "X-RapidAPI-Key":
            "9ebbae1af8msh23c669d14ec6f94p176eacjsn9d72df12550c",
          //"X-RapidAPI-Key" : "a4cc668aedmsha3091c6e65ccf98p1a191djsnfefa2730daf0",
          "X-RapidAPI-Host": "streaming-availability.p.rapidapi.com",
        },
      });

      if (!response.ok)
        throw new Error("Errore nel recupero dei risultati della ricerca");
      const { result } = await response.json();

      setList(result); //Aggiorno il contesto con i nuovi risultati
      navigate("/list"); //Naviga alla pagina dei risultati
    } catch (error) {
      console.error(error.message);
    }
  };

  const handleDeleteClick = async (id, event) => {
    event.stopPropagation(); //Previene il trigger dell'evento di click della card
    try {
      const response = await fetch(
        `http://localhost:8080/api/streamInfo/delete/${id}`,
        {
          method: "DELETE",
        }
      );
      if (!response.ok) {
        throw new Error("Errore nella cancellazione");
      }

      setRicerche(ricerche.filter((ricerca) => ricerca.id !== id));
      alert("Ricerca cancellata");
      navigate("/userResearch");
    } catch (error) {
      console.error(error);
    }
  };

  return (
    <>
      <style>{`
        .card-title {
          cursor: pointer;
        }
        .card-title:hover {
          color: #017cff; 
          text-decoration: underline; 
        }
        .card {
          transition: box-shadow 0.3s;
        }
        .card:hover {
          box-shadow: 0 5px 15px rgba(0, 0, 0, 0.1); 
        }
        .delete-button {
          position: absolute;
          top: 10px;
          right: 10px;
          cursor: pointer;
        }
      `}</style>
      <div className="container mt-5">
        <h2 className="mb-4 text-center" style={{ color: "white" }}>
          La tua cronologia di ricerca
        </h2>
        <div className="row">
          {ricerche.length > 0 ? (
            ricerche.map((ricerca, index) => (
              <div key={ricerca.id} className="col-lg-6 mb-3">
                <div
                  className="card h-100 position-relative"
                  onClick={() => handleCardClick(ricerca.title, ricerca.geo)}
                >
                  <button
                    className="btn btn-danger delete-button"
                    onClick={(event) => handleDeleteClick(ricerca.id, event)}
                  >
                    X
                  </button>
                  <div className="card-body">
                    <h5 className="card-title">{ricerca.title}</h5>
                    <p className="card-text">
                      Geo: {ricerca.geo.toUpperCase()}
                    </p>
                  </div>
                </div>
              </div>
            ))
          ) : (
            <p className="text-center">Nessuna ricerca trovata.</p>
          )}
        </div>
      </div>
    </>
  );
}
