import React, { useContext } from "react";
import { ResearchContext } from "../context/ResearchContext";

export default function Details() {
  const {
    list,
    setList,
    item,
    setItem,
    geo,
    setGeo,
    userData,
    setUserData,
    isLogged,
    setIsLogged,
    isAdmin,
    setIsAdmin,
  } = useContext(ResearchContext);

  //Se l'item passato per la visione del dettaglio è vuoto viene visualizzato questo messaggio
  if (!item) {
    return <div>Nessun dettaglio disponibile</div>;
  }

  //Gestione della visualizzazione delle informazioni di streaming
  const streamingInfo =
    item.streamingInfo && item.streamingInfo[geo]
      ? item.streamingInfo[geo].map((info, index) => (
          <p key={index}>
            <strong>Service:</strong> {info.service} -{" "}
            <strong>Streaming type:</strong> {info.streamingType}
            <br />
            <strong>Quality:</strong> {info.quality} <strong>Audio:</strong>{" "}
            {info.audios.map((audio) => audio.language).join(", ")}
            <br />
            <strong>Link:</strong>{" "}
            <a href={info.link} target="_blank">
              Guarda ora
            </a>
          </p>
        ))
      : "Informazioni di streaming non disponibili.";

  const cast = item.cast?.length
    ? item.cast.join(", ")
    : "Cast non disponibile";
  const genres =
    item.genres?.map((genre) => genre.name).join(", ") ||
    "Generi non disponibili";

  return (
    <div>
      {isLogged ? (
        <div className="container mt-5">
          <div className="row">
            <div className="col-lg-8 mx-auto">
              <div className="card shadow-lg mb-5">
                <div className="card-header bg-dark text-white text-center p-4">
                  <h1 className="card-title mb-0">{item.title}</h1>
                </div>
                <div className="card-body">
                  <div className="row">
                    {/* Colonna per la descrizione */}
                    <div className="col-md-6">
                      <p>{item.overview}</p>
                      <p>
                        <strong>Cast:</strong> {cast}
                      </p>
                      <p>
                        <strong>Episodi:</strong> {item.episodeCount}
                      </p>
                      <p>
                        <strong>Stagioni:</strong> {item.seasonCount}
                      </p>
                      <p>
                        <strong>Genere:</strong> {genres}
                      </p>
                      <p>
                        <strong>Titolo Originale:</strong> "{item.originalTitle}
                        "
                      </p>
                      <p>
                        <strong>Status:</strong> {item.status?.statusText}
                      </p>
                    </div>
                    {/* Colonna per i link e altre informazioni */}
                    <div className="col-md-6">{streamingInfo}</div>
                  </div>
                </div>
                <div className="card-footer text-muted text-center">
                  <small>Last updated 3 mins ago</small>
                </div>
              </div>
            </div>
          </div>
        </div>
      ) : (
        <p>Effettua prima la login!</p>
      )}
    </div>
  );
}
