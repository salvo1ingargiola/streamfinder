import FormLogin from "../components/FormLogin";
import { useContext, useEffect, useState } from "react";
import { ResearchContext } from "../context/ResearchContext";

export default function CardProfile() {
  const {
    list,
    setList,
    item,
    setItem,
    geo,
    setGeo,
    userData,
    setUserData,
    isLogged,
    setIsLogged,
    isAdmin,
    setIsAdmin,
  } = useContext(ResearchContext);

  //Se l'utente è loggato mostro la card con il suo profilo, altrimenti mostro il form di login
  return (
    <>
      <h1 className="card-title" style={{ color: "white" }}>
        Benvenuto al profilo utente
      </h1>
      <br />
      {isLogged ? (
        <div className="container">
          <div className="card">
            <div className="card-body">
              <p className="card-text">
                <strong>Nome:</strong> {userData.nome}
              </p>
              <p className="card-text">
                <strong>Cognome:</strong> {userData.cognome}
              </p>
              <p className="card-text">
                <strong>Email:</strong> {userData.email}
              </p>
            </div>
          </div>
        </div>
      ) : (
        <FormLogin />
      )}
    </>
  );
}
