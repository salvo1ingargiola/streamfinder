import { useContext } from "react";
import { ResearchContext } from "../context/ResearchContext";
import { useNavigate } from "react-router-dom";

export default function List() {
  const {
    list,
    setList,
    item,
    setItem,
    geo,
    setGeo,
    userData,
    setUserData,
    isLogged,
    setIsLogged,
    isAdmin,
    setIsAdmin,
  } = useContext(ResearchContext);
  const navigate = useNavigate();

  //Questa funzione restituisce un'altra funzione che gestisce il click su una card specifica
  //e si assicura che l'item corretto sia passato al momento del click
  const handleCardClick = (item) => () => {
    setItem(item); // Imposta l'item nel contesto
    navigate("/details"); // Naviga alla pagina dei dettagli
  };

  return (
    <div>
      {isLogged ? (
        <div>
          <style>{`
        .clickable-card {
          cursor: pointer;
        }
        
        .clickable-card:hover {
          box-shadow: 0 0 5px rgba(0, 0, 0, 0.2);
        }
      `}</style>
          <div className="container mt-5">
            <div className="row">
              {list.map((item, index) => (
                //Utilizzo un div invece di Link per rendere l'intera card cliccabile e gestisco il click sulla card per navigare
                <div key={index} className="col-md-4 mb-3">
                  <div
                    className="card h-100 clickable-card"
                    onClick={handleCardClick(item)}
                  >
                    <div className="card-body">
                      <h5 className="card-title">{item.title}</h5>
                      <p className="card-text">{item.overview}</p>
                    </div>
                    <div className="card-footer">
                      <small className="text-muted">
                        Cast: {item.cast.join(", ")}
                      </small>
                    </div>
                  </div>
                </div>
              ))}
            </div>
          </div>
        </div>
      ) : (
        <p>Effettua prima la login!</p>
      )}
    </div>
  );
}
